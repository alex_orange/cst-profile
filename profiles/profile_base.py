import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum

DATASET = "urn:publicid:IDN+emulab.net:powderteam+ltdataset+CST-install-v2"

def make_profile(disk_image, read_only=True, run_startup_scripts=True):
    portal.context.defineParameter("node_id",
                                   "ID of the node to get",
                                   portal.ParameterType.STRING, "")

    params = portal.context.bindParameters()
    request = portal.context.makeRequestRSpec()

    node = request.RawPC("node")
    node.component_id = params.node_id
    node.disk_image = disk_image

    if run_startup_scripts:
        node.addService(
            rspec.Execute(shell="bash",
                          command="/local/repository/start_vnc.sh"))

    iface = node.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/opt/cst")
    fsnode.dataset = DATASET
    fsnode.readonly = read_only

    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)

    fslink.best_effort = True
    fslink.vlan_tagging = True

    portal.context.printRequestRSpec()
